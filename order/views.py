from django.shortcuts import redirect

from cart.cart import Cart

from .models import Order, OrderItem


def start_order(request):
    cart = Cart(request)

    if request.method == "POST":
        first_name = request.POST["first_name"]
        last_name = request.POST["last_name"]
        email = request.POST["email"]
        phone = request.POST["phone"]
        address = request.POST["address"]
        city = request.POST["city"]
        state = request.POST["state"]
        zipcode = request.POST["zipcode"]

        total_amount = sum(
            item["product"].price * int(item["quantity"]) for item in cart
        )

        order = Order.objects.create(
            user=request.user,
            first_name=first_name,
            last_name=last_name,
            email=email,
            phone=phone,
            address=address,
            city=city,
            state=state,
            zipcode=zipcode,
            paid_amount=total_amount,
            paid=True,
        )

        for item in cart:
            product = item["product"]
            quantity = int(item["quantity"])
            price = product.price * quantity
            item = OrderItem.objects.create(
                order=order, product=product, price=price, quantity=quantity
            )

        cart.clear()
        return redirect("checkout_success")

    return redirect("cart")
