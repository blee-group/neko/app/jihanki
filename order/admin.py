from django.contrib import admin

from .models import Order, OrderItem


# create tabular inline to show OrderItem in the Order admin page
class OrderItemInline(admin.TabularInline):
    model = OrderItem
    raw_id_fields = ["product"]


class OrderAdmin(admin.ModelAdmin):
    # fields to include in the admin display
    list_display = [
        "id",
        "first_name",
        "last_name",
        "created_at",
        "email",
        # "address",
        # "zipcode",
        # "city",
        "status",
    ]
    list_filter = ["status", "created_at"]  # enable filters in the admin interface
    search_fields = [
        "first_name",
        "last_name",
        "email",
    ]  # enable search in the admin interface
    inlines = [OrderItemInline]


admin.site.register(Order, OrderAdmin)
admin.site.register(OrderItem)
