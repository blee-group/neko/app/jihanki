from django.urls import path
from django.contrib.auth import views

from core.views import home, shop, signup, account, edit_account
from catalog.views import catalog

urlpatterns = [
    path("", home, name="home"),
    path("signup/", signup, name="signup"),
    path(
        "login/", views.LoginView.as_view(template_name="core/login.html"), name="login"
    ),
    path("account/", account, name="account"),
    path("account/edit", edit_account, name="edit_account"),
    path("logout/", views.LogoutView.as_view(), name="logout"),
    path("shop/", shop, name="shop"),
    path("shop/<slug:slug>", catalog, name="catalog"),
]
