from django.conf import settings

from catalog.models import Product


class Cart(object):
    def __init__(self, request):
        self.session = request.session
        cart = self.session.get(settings.CART_SESSION_ID)

        if not cart:
            cart = self.session[settings.CART_SESSION_ID] = {}

        self.cart = cart

    def __iter__(self):
        for p in self.cart.keys():
            self.cart[str(p)]["product"] = Product.objects.get(pk=p)

        for item in self.cart.values():
            item["total_price"] = item["product"].price * item["quantity"]
            yield item

    # check length of products in cart
    def __len__(self):
        return sum(item["quantity"] for item in self.cart.values())

    # notify browser when cart updated
    def save(self):
        self.session[settings.CART_SESSION_ID] = self.cart
        self.session.modified = True

    # create method for adding products to cart
    def add(self, product_id, quantity=1, update_quantity=False):
        product_id = str(product_id)

        if product_id not in self.cart:
            self.cart[product_id] = {"quantity": 1, "id": product_id}

        if update_quantity:
            self.cart[product_id]["quantity"] += int(quantity)

            # if  is 0, remove product from cart
            if self.cart[product_id]["quantity"] == 0:
                self.remove(product_id)
        self.save()

    # add method to remove products from cart
    def remove(self, product_id):
        if product_id in self.cart:
            del self.cart[product_id]
            self.save()

    # add method to clear cart
    def clear(self):
        del self.session[settings.CART_SESSION_ID]
        self.session.modified = True
        
    def get_total_price(self):
        total_price = 0
        for item in self.cart.values():
            product_id = item.get("id")
            if product_id:
                try:
                    product = Product.objects.get(pk=product_id)
                    total_price += product.price * item["quantity"]
                except Product.DoesNotExist:
                    pass  # Handle the case where product doesn't exist
        return total_price

    def get_item(self, product_id):
        if str(product_id) in self.cart:
            return self.cart[str(product_id)]
        else:
            return None
        # return self.cart[str(product_id)]
