from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from .cart import Cart

from catalog.models import Product


def add_to_cart(request, product_id):
    cart = Cart(request)
    cart.add(product_id)

    return render(request, "cart/partials/menu_cart.html")


def cart(request):
    return render(request, "cart/cart.html")


# create a view for checkout success


@login_required
def checkout_success(request):
    return render(request, "cart/checkout_success.html")


@login_required
def checkout(request):
    return render(request, "cart/checkout.html")


def hx_menu_cart(request):
    return render(request, "cart/partials/menu_cart.html")


def hx_cart_total(request):
    return render(request, "cart/partials/cart_total.html")


# TODO: clean this update_cart function up
def update_cart(request, product_id, action):
    cart = Cart(request)

    if action == "increment":
        cart.add(product_id, 1, True)
    elif action == "decrement":
        cart.add(product_id, -1, True)

    product = Product.objects.get(pk=product_id)
    quantity = cart.get_item(product_id)

    if quantity:
        quantity = quantity["quantity"]

        item = {
            "product": {
                "id": product.id,
                "slug": product.slug,
                "name": product.name,
                "image": product.image,
                "get_thumbnail": product.get_thumbnail(),
                "price": product.price,
            },
            "total_price": product.price * quantity,
            "quantity": quantity,
        }
    else:
        item = None

    response = render(request, "cart/partials/cart_item.html", {"item": item})
    response["HX-Trigger"] = "update-menu-cart"

    return response
