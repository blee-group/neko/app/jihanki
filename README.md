# The Cat Vending Machine

Demo Project

## Developing

Local development requires Docker and Docker Compose

### Docker Compose

`docker-compose.yml` is set up for local development. The `Dockerfile` provides
instructions for containerized deployment, Postgres support from env vars
is enabled in `settings.py`.

To launch dev environment:

```shell
docker-compose up -d --build
```

Django will run in the `web` service:

```shell
docker-compose run web python manage.py <commands>
```

### Non Docker Alternative

Settings should enable almost full functionality in a virtual environment.
Postgres settings will need to be configured to launch properly.

#### Create Virtual Environment

```shell
virtualenv venv
```

#### Activate Virtual Environment

```shell
source venv/bin/activate
```

#### Install deps

```shell
pip install -r requirements.txt
cd jihanki/theme/static_src
npm install
```

#### Start environment

```shell
cd <projectroot>
python3 manage.py makemigrations
python3 manage.py migrate
python3 manage.py tailwind start
python3 manage.py runserver
```

## Building For Prod

The `Dockerfile` handles the `django-tailwind` and `collectstatic` jobs.

To manually build for prod, you can minify the css file before committing:

```shell
python3 manage.py tailwind build
```

## django-tailwind

[project home](https://github.com/timonweb/django-tailwind)
